FROM php:7.4-apache
MAINTAINER Cam O "<webcam@smokeylemon.com>"
ENV DEBIAN_FRONTEND=noninteractive

# Install components
RUN apt-get update -y && \
    apt-get install -y \
        libzip-dev \
        memcached \
        curl \
        netcat \
        telnetd \
        git-core \
        gzip \
        libcurl4-openssl-dev \
        libgd-dev \
        libldap2-dev \
        libtidy-dev \
        libxslt-dev \
        zlib1g-dev \
        libicu-dev \
        g++ \
        openssh-client \
        libmagickwand-dev \
        unzip \
        zip \
        default-mysql-client \
        --no-install-recommends && \
    curl -sS https://silverstripe.github.io/sspak/install | php -- /usr/local/bin && \
    curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer && \
    pecl install xdebug && \
    pecl install imagick-3.4.3 && \
    apt-get autoremove -y && \
    rm -r /var/lib/apt/lists/*
    
RUN apt-get update -y && \
    apt-get install -y libmcrypt-dev && \
    pecl install mcrypt && \
    docker-php-ext-install mysqli && \
    docker-php-ext-enable mysqli && \
    docker-php-ext-enable mcrypt
    
 
RUN apt-get update &&\
    apt-get install --no-install-recommends --assume-yes --quiet ca-certificates curl git &&\
    rm -rf /var/lib/apt/lists/*
RUN curl -Lsf 'https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz' | tar -C '/usr/local' -xvzf -
ENV PATH /usr/local/go/bin:$PATH
RUN go get github.com/mailhog/mhsendmail
RUN cp /root/go/bin/mhsendmail /usr/bin/mhsendmail
RUN echo 'sendmail_path = /usr/bin/mhsendmail --smtp-addr mail:1025' > /usr/local/etc/php/php.ini    

# Install PHP Extensions
RUN docker-php-ext-configure intl && \
    docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
    docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-enable xdebug imagick mcrypt && \
    sed -i '1 a xdebug.remote_autostart=trigger' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    sed -i '1 a xdebug.start_with_request=req' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    sed -i '1 a xdebug.remote_handler=dbgp' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    sed -i '1 a xdebug.output_dir="/tmp/xdebug/"' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    sed -i '1 a xdebug.profiler_output_dir="/tmp/xdebug/"' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    sed -i '1 a xdebug.mode=debug,profile,trace' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    sed -i '1 a xdebug.idekey=PHPSTORM' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    sed -i '1 a xdebug.client_host=host.docker.internal' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    sed -i '1 a xdebug.profiler_output_name="cachegrind.out.%s"' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    docker-php-ext-install -j$(nproc) \
        intl \
        gd \
        ldap \
        pdo \
        pdo_mysql \
        soap \
        tidy \
        xsl \
        zip \
        opcache

# Apache + xdebug configuration
RUN { \
        echo "<VirtualHost *:80>"; \
        echo "  DocumentRoot /var/www/public"; \
        echo "  LogLevel warn"; \
        echo "  ErrorLog /var/log/apache2/error.log"; \
        echo "  CustomLog /var/log/apache2/access.log combined"; \
        echo "  ServerSignature Off"; \
        echo "  <Directory /var/www/public>"; \
        echo "    Options +FollowSymLinks"; \
        echo "    Options -ExecCGI -Includes -Indexes"; \
        echo "    AllowOverride all"; \
        echo; \
        echo "    Require all granted"; \
        echo "  </Directory>"; \
        echo "  <LocationMatch assets/>"; \
        echo "    php_flag engine off"; \
        echo "  </LocationMatch>"; \
        echo; \
        echo "  IncludeOptional sites-available/000-default.local*"; \
        echo "</VirtualHost>"; \
    } | tee /etc/apache2/sites-available/000-default.conf

RUN echo "ServerName localhost" > /etc/apache2/conf-available/fqdn.conf && \
    echo "date.timezone = Pacific/Auckland" > /usr/local/etc/php/conf.d/timezone.ini && \
    a2enmod rewrite expires remoteip cgid && \
    a2enmod headers && \
    usermod -u 1000 www-data && \
    usermod -G staff www-data

#RUN sed 's/main$/main universe/' -i /etc/apt/sources.list
#RUN apt-get update
#RUN apt-get upgrade -y

# Download and install wkhtmltopdf
RUN apt-get update
RUN apt-get install -y wkhtmltopdf xvfb xauth xfonts-base xfonts-75dpi fontconfig
RUN echo 'xvfb-run --server-args="-screen 0, 1024x768x24" /usr/bin/wkhtmltopdf $*' > /usr/bin/wkhtmltopdf.sh
RUN chmod a+rx /usr/bin/wkhtmltopdf.sh
RUN ln -s /usr/bin/wkhtmltopdf.sh /usr/local/sbin/wkhtmltopdf

RUN composer config -g http-basic.toranproxy.smokeylemon.com smokeylemon smokey2009
#RUN /usr/local/sbin/wkhtmltopdf https://www.google.fr output.pdf
#RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.stretch_amd64.deb
#RUN gdebi --n wkhtmltox-0.12.6-1_linux-trusty-amd64.deb
#ENTRYPOINT ["wkhtmltopdf"]

# Install APCu and APC backward compatibility
RUN pecl install apcu \
    && pecl install apcu_bc-1.0.3 \
    && docker-php-ext-enable apcu --ini-name 10-docker-php-ext-apcu.ini \
    && docker-php-ext-enable apc --ini-name 20-docker-php-ext-apc.ini


EXPOSE 80
CMD ["apache2-foreground"]